package redpi.apps.ushare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ClipboardManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.util.ArrayList;

import redpi.apps.tables.FileInfo;

/**
 * Created by Nikhil on 01-08-2015.
 */
public class ListAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity mActivity;
    private ArrayList<ListItemData> dataList;
    private LayoutInflater inflater=null;
    public ListAdapter(Activity activity, ArrayList d) {

        mActivity = activity;
        dataList=d;
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.copyLink:
                final String copyUrl = (String) v.getTag();
                ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("url",copyUrl);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mActivity,"Url copied to clipboard",Toast.LENGTH_SHORT).show();
                break;
            case R.id.shareLink:
                final String shareUrl = (String) v.getTag();
                if(MainActivity.appNames.size() != 0) {
                    ShareListAdapter adapter = new ShareListAdapter(mActivity, MainActivity.appNames, MainActivity.appIcons);

                    new AlertDialog.Builder(mActivity).setTitle("Share with...")
                            .setAdapter(adapter, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int item ) {
                                    if(item>=MainActivity.packageToIndexMap.size()){
                                        defaultShareIntent(shareUrl,null);
                                    }else {
                                        defaultShareIntent(shareUrl, MainActivity.packageNames[MainActivity.packageToIndexMap.get(item)]);
                                    }
                                }
                            }).show();
                }else {
                    defaultShareIntent(shareUrl,null);
                }
                break;
            case R.id.deleteLink:
                if(!MainActivity.isNetworkAvailable(mActivity)){
                    Toast.makeText(mActivity,"No internet Connection",Toast.LENGTH_SHORT).show();
                    return;
                }
                final ListItemData dataItem = (ListItemData) v.getTag();
                RequestParams params = new RequestParams();
                params.put(MainActivity.DELETE_FILE, true);
                params.put(MainActivity.FILE_NAME, dataItem.fileName);
                MainActivity.client.post(MainActivity.SERVER_UPLOAD_URL_RELEASE, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        dataList.remove(dataItem);
                        notifyDataSetChanged();
                       new Thread(){
                           @Override
                           public void run() {
                               super.run();
                               MainActivity.db.delete(FileInfo.TABLE_NAME, FileInfo.FileInfoColumns._ID + " =?", new String[]{String.valueOf(dataItem.rowId)});

                           }
                       }.start();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(mActivity,"Failed to delete file.Try again later",Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case R.id.retryUpload:
                if(((MainActivity)mActivity).isNetworkAvailable(mActivity)) {
                    ListItemData dataItem2 = (ListItemData) v.getTag();
                    ((MainActivity) mActivity).patch(dataItem2);
                }else{
                    Toast.makeText(mActivity,"No internet connection",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.deleteUpload:
                final ListItemData dataItem3 = (ListItemData)v.getTag();
                dataList.remove(dataItem3);
                notifyDataSetChanged();
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        MainActivity.db.delete(FileInfo.TABLE_NAME, FileInfo.FileInfoColumns._ID + " = ?", new String[]{String.valueOf(dataItem3.rowId)});
                    }
                }.start();

                break;

            case R.id.downloadLink:
                String url = (String) v.getTag();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                mActivity.startActivity(i);
                break;

        }
    }

    private void defaultShareIntent(String shareUrl,String packageName){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
        if(packageName!=null){
            sendIntent.setPackage(packageName);
        }
        sendIntent.setType("text/plain");
        try {
            mActivity.startActivity(sendIntent);
        }catch (ActivityNotFoundException e){
           Toast.makeText(mActivity,mActivity.getString(R.string.could_not_find),Toast.LENGTH_SHORT).show();
        }
    }


    private class ViewHolder{
        TextView fileName;
        TextView url;
        TextView uploadingText;
        RoundCornerProgressBar progressBar;
        BootstrapButton copyLink;
        BootstrapButton shareLink;
        BootstrapButton deleteLink;
        BootstrapButton download;
        BootstrapButton retryUpload;
        BootstrapButton deleteUpload;
        LinearLayout buttonsCointainer;
        LinearLayout retryContainer;




    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.list_view_item,null);
            holder = new ViewHolder();
            holder.fileName = (TextView) convertView.findViewById(R.id.fileName);
            holder.url = (TextView) convertView.findViewById(R.id.fileUrl);
            holder.uploadingText = (TextView) convertView.findViewById(R.id.uploading);
            holder.progressBar = (RoundCornerProgressBar) convertView.findViewById(R.id.progress);
            holder.copyLink = (BootstrapButton) convertView.findViewById(R.id.copyLink);
            holder.shareLink = (BootstrapButton) convertView.findViewById(R.id.shareLink);
            holder.deleteLink = (BootstrapButton) convertView.findViewById(R.id.deleteLink);
            holder.retryUpload = (BootstrapButton) convertView.findViewById(R.id.retryUpload);
            holder.deleteUpload = (BootstrapButton)convertView.findViewById(R.id.deleteUpload);
            holder.download = (BootstrapButton)convertView.findViewById(R.id.downloadLink);

            holder.buttonsCointainer  = (LinearLayout) convertView.findViewById(R.id.buttonsContainer);
            holder.retryContainer  = (LinearLayout) convertView.findViewById(R.id.retryContainer);

            holder.copyLink.setOnClickListener(this);
            holder.shareLink.setOnClickListener(this);
            holder.deleteLink.setOnClickListener(this);
            holder.retryUpload.setOnClickListener(this);
            holder.deleteUpload.setOnClickListener(this);
            holder.download.setOnClickListener(this);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        ListItemData dataItem = dataList.get(position);

        holder.fileName.setText(dataItem.fileName);
        holder.deleteUpload.setTag(dataItem);

        if(dataItem.uploadStatus == MainActivity.UPLAOD_COMPRESSING){
            holder.uploadingText.setVisibility(View.VISIBLE);
            holder.uploadingText.setText("Compressing files...");
            holder.progressBar.setVisibility(View.GONE);
            holder.retryContainer.setVisibility(View.GONE);
            holder.buttonsCointainer.setVisibility(View.GONE);
            holder.url.setVisibility(View.GONE);
        }
        else if(dataItem.uploadStatus == MainActivity.UPLAOD_CONNECTING){
            holder.uploadingText.setVisibility(View.VISIBLE);
            holder.uploadingText.setText("Connecting...");
            holder.progressBar.setVisibility(View.GONE);
            holder.retryContainer.setVisibility(View.GONE);
            holder.buttonsCointainer.setVisibility(View.GONE);
            holder.url.setVisibility(View.GONE);



        }else if(dataItem.uploadStatus == MainActivity.UPLAOD_IN_PROGESS){
            holder.uploadingText.setVisibility(View.VISIBLE);
            holder.uploadingText.setText("Uploading...");
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.progressBar.setProgress(dataItem.progress);
            holder.retryContainer.setVisibility(View.GONE);
            holder.buttonsCointainer.setVisibility(View.GONE);
            holder.url.setVisibility(View.GONE);



        }else if(dataItem.uploadStatus == MainActivity.UPLAOD_SUCCESS){
            holder.uploadingText.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.GONE);
            holder.progressBar.setProgress(0);
            holder.url.setText(dataItem.fileUrl);
            holder.copyLink.setTag(dataItem.fileUrl);
            holder.shareLink.setTag(dataItem.fileUrl);
            holder.download.setTag(dataItem.fileUrl);
            holder.deleteLink.setTag(dataItem);
            holder.url.setVisibility(View.VISIBLE);
            holder.buttonsCointainer.setVisibility(View.VISIBLE);
            holder.retryContainer.setVisibility(View.GONE);

        }else if(dataItem.uploadStatus==MainActivity.UPLAOD_FAILED){
            holder.uploadingText.setVisibility(View.VISIBLE);
            holder.uploadingText.setText("Failed to upload");
            holder.progressBar.setVisibility(View.GONE);
            holder.progressBar.setProgress(0);
            holder.retryContainer.setVisibility(View.VISIBLE);
            holder.retryUpload.setTag(dataItem);
            holder.buttonsCointainer.setVisibility(View.GONE);
            holder.url.setVisibility(View.GONE);


        }
       /* if(TextUtils.isEmpty(dataItem.fileUrl)){
            holder.progressBar.setProgress(dataItem.progress);
        }else{
            holder.uploadingText.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.GONE);
            holder.url.setText(dataItem.fileUrl);
            holder.copyLink.setTag(dataItem.fileUrl);
            holder.shareLink.setTag(dataItem.fileUrl);
            holder.deleteLink.setTag(dataItem.fileName);
            holder.url.setVisibility(View.VISIBLE);
            holder.buttonsCointainer.setVisibility(View.VISIBLE);
        }*/
        return convertView;
    }
}
