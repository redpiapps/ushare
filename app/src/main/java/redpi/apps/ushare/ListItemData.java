package redpi.apps.ushare;

import android.net.Uri;

import java.io.File;

/**
 * Created by Nikhil on 01-08-2015.
 */
public class ListItemData {

    public String fileName;
    public String fileUrl;
    public int progress;
    public int uploadStatus;
    public long rowId = -1;
    public Uri uriToUpload = null;
}
