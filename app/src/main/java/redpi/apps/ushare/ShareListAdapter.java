package redpi.apps.ushare;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nikhil on 06-08-2015.
 */
public class ShareListAdapter extends BaseAdapter {

    ArrayList<Drawable> icons;
    ArrayList<String> appNames;
    Activity activity;
    public ShareListAdapter(Activity activity,ArrayList<String> appNames,ArrayList<Drawable> icons) {
        this.icons = icons;
        this.appNames = appNames;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return icons.size();
    }

    @Override
    public Object getItem(int i) {
        return appNames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.share_picker_item,null);
        TextView appName = (TextView) view.findViewById(R.id.appName);
        ImageView appIcon = (ImageView) view.findViewById(R.id.shareAppIcon);
        appName.setText(appNames.get(position));
        appIcon.setImageDrawable(icons.get(position));
       /* textView.setCompoundDrawablesWithIntrinsicBounds(icons.get(position), null, null, null);
        textView.setCompoundDrawablePadding(
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getContext().getResources().getDisplayMetrics()));*/
        return view;
    }


}
