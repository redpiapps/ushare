package redpi.apps.ushare;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import redpi.apps.tables.FileInfo;

/**
 * Created by Nikhil on 01-08-2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "UShare_database";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_FILE_TABLE =
            "CREATE TABLE " + FileInfo.TABLE_NAME + " (" +
                    FileInfo.FileInfoColumns._ID + INT_TYPE + " PRIMARY KEY," +
                    FileInfo.FileInfoColumns.FILENAME + TEXT_TYPE + COMMA_SEP +
                    FileInfo.FileInfoColumns.URL + TEXT_TYPE + COMMA_SEP +
                    FileInfo.FileInfoColumns.UPLOAD_STATUS + INT_TYPE + COMMA_SEP +
                    FileInfo.FileInfoColumns.URI_TO_UPLOAD + TEXT_TYPE + COMMA_SEP +
                    FileInfo.FileInfoColumns.PROGRESS + INT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FileInfo.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FILE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
