package redpi.apps.ushare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionsMenu;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import com.nononsenseapps.filepicker.FilePickerActivity;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import redpi.apps.tables.FileInfo;
import redpi.apps.tables.FileInfo.FileInfoColumns;
import redpi.apps.utilities.ZipUtil;

import com.getbase.floatingactionbutton.FloatingActionButton;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, Picker.PickListener {

    private static final int PICKFILE_REQUEST_CODE = 1;
    private static final int PICKIMAGE_REQUEST_CODE = 2;
    private static final int PICKVIDEO_REQUEST_CODE = 3;
    private static final String FILE_TO_UPLOAD = "fileToUpload";
    public static final String DELETE_FILE = "deleteFile";
    public static final String FILE_NAME = "fileName";
    private static final int LOAD_LIST_DATA = 0;
    private FloatingActionButton addNewFile;
    private FloatingActionButton addNewImage;
    private FloatingActionButton addNewVideo;
    private FloatingActionsMenu floatingMenu;
    public static final String SERVER_UPLOAD_URL_DEBUG = "http://192.168.1.108/UShare_server/fileUpload.php";
    public static final String SERVER_UPLOAD_URL_RELEASE = "http://filebasket.in/fileUpload.php";

    private static final String EXTERNAL_STORAGE_DIR = Environment.getExternalStorageDirectory().getPath() + "/";
    private static final String ZIP_FILENAME = "fileBasket.zip";


    public static AsyncHttpClient client = new AsyncHttpClient();
    private ListView filesListView;
    private ArrayList<ListItemData> dataList;
    private ListAdapter listAdapter;
    public static final int UPLAOD_FAILED = 0;
    public static final int UPLAOD_SUCCESS = 1;
    public static final int UPLAOD_IN_PROGESS = 2;
    public static final int UPLAOD_CONNECTING = 3;
    public static final int UPLAOD_COMPRESSING = 4;

    Animation fadeIn;
    Animation fadeOut;
    public static SQLiteDatabase db;
    public static DatabaseHelper mDbHelper;
    public View dummyBg;
    private Uri fileUriFromContentUri;
    private AlertDialog alertDialog;
    public static ArrayList<String> appNames;
    public static ArrayList<Drawable> appIcons;
    public static final String[] packageNames = {"com.whatsapp","com.facebook.orca","jp.naver.line.android","com.facebook.katana","com.bsb.hike"};
    public static ArrayList<Integer> packageToIndexMap ;


    @Override
    protected void onPause() {
        super.onPause();
        //client.cancelAllRequests(true);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                getSupportActionBar().setElevation(0);
            }
        } catch (Exception e) {

        }
        initVariables();
        initListeners();
        initList();
        checkForIntent();
        initSharePackages();


    }

    private void initSharePackages() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                PackageManager pm = getPackageManager();
                ApplicationInfo info;
                for(int i=0;i<packageNames.length;++i){
                    if(isPackageInstalled(MainActivity.this,packageNames[i])){
                        try {
                            info = pm.getApplicationInfo(packageNames[i],0);
                            appIcons.add(pm.getApplicationIcon(info));
                            appNames.add((String) pm.getApplicationLabel(info));
                            packageToIndexMap.add(i);
                        }catch (PackageManager.NameNotFoundException nnfe){

                        }
                    }
                }
                if(appNames.size()>0){
                    appIcons.add(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_menu_more));
                    appNames.add("Other...");
                }
            }
        }).start();

    }

    private void checkForIntent() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        if (intent.getAction() == null) {
            return;
        }
        try {
            if (Intent.ACTION_SEND.equals(intent.getAction())) {
                Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                uri = convertContentUriToFileUri(uri);
                post(uri, null);
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(intent.getAction())) {
                ArrayList<Uri> uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                ArrayList<String> filePaths = new ArrayList<String>();

                if (uris != null) {
                    for (Uri uri : uris) {
                        filePaths.add(convertContentUriToFileUri(uri).getPath());
                    }
                }
                initDialogWithPath(filePaths);
            }
        }catch (Exception e){
            Toast.makeText(this,"Failed to upload file.",Toast.LENGTH_LONG).show();
        }

    }

    private Uri convertContentUriToFileUri(Uri uri) {
        if (uri != null && "content".equals(uri.getScheme())) {
            Cursor cursor = this.getContentResolver().query(uri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            String filePath = cursor.getString(0);
            cursor.close();
            uri = Uri.parse(filePath);
        }
        return uri;
    }

    public void initDialogWithPath(final ArrayList<String> pathList) {
        alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).
                setTitle(pathList.size() + " files selected").
                setMessage("Do you want to upload them as seperate files or an individual zip file ?").
                setPositiveButton("Seperate Files", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (String path : pathList) {
                            post(Uri.parse(path), null);
                        }
                    }
                }).
                setNegativeButton("Single File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.cancel();
                        File zipFile = createZipFile();
                        ListItemData dataItem = addZipFileToList(zipFile);
                        new ZipTask(zipFile, dataItem).execute(pathList);
                        //ZipUtil.createZipWithPaths(pathList, createZipFile());
                    }
                }).
                show();

    }


    public void initDialog(final ArrayList<ImageEntry> arrayList) {
        alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).
                setTitle(arrayList.size() + " files selected").
                setMessage("Do you want to upload them as seperate files or an individual zip file ?").
                setCancelable(true).
                setPositiveButton("Seperate Files", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.cancel();
                        for (ImageEntry entry : arrayList) {
                            post(Uri.parse(entry.path), null);
                        }
                    }
                }).
                setNegativeButton("Single File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alertDialog.cancel();
                        File zipFile = createZipFile();
                        ListItemData dataItem = addZipFileToList(zipFile);
                        new ZipTask(zipFile, dataItem).execute(arrayList);
                        //post(ZipUtil.createZip(arrayList, zipFile));
                    }
                }).
                show();

    }

    public ListItemData addZipFileToList(File zipFile) {
        ListItemData data = new ListItemData();
        data.fileName = zipFile.getName();
        data.uploadStatus = UPLAOD_COMPRESSING;
        dataList.add(0, data);
        listAdapter.notifyDataSetChanged();
        return data;
    }


    private class ZipTask extends AsyncTask<ArrayList, Void, Void> {

        Uri zipFileUri;
        File zipFile;
        ListItemData dataItem;

        public ZipTask(File zipFile, ListItemData dataItem) {
            this.dataItem = dataItem;
            this.zipFile = zipFile;
        }

        @Override
        protected Void doInBackground(ArrayList... list) {
            zipFileUri = ZipUtil.createZip(list[0], zipFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            post(zipFileUri, dataItem);

        }
    }

    private class ZipTaskForClipData extends AsyncTask<ClipData, Void, Void> {

        Uri zipFileUri;
        File zipFile;
        ListItemData dataItem;

        public ZipTaskForClipData(File zipFile, ListItemData dataItem) {
            this.zipFile = zipFile;
            this.dataItem = dataItem;
        }

        @Override
        protected Void doInBackground(ClipData... list) {
            zipFileUri = ZipUtil.createZip(list[0], zipFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            post(zipFileUri, dataItem);


        }
    }


    public File createZipFile() {
        File zipFile = new File(EXTERNAL_STORAGE_DIR + ZIP_FILENAME);
        int count = 1;
        while (zipFile.exists()) {
            zipFile = new File(EXTERNAL_STORAGE_DIR + (count + "_" + ZIP_FILENAME));
            count++;
        }
        return zipFile;
    }

    public void initDialog(final ClipData clipData) {
        final int count = clipData.getItemCount();
        alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).
                setTitle(count + " files selected").
                setMessage("Do you want to upload them as seperate files or an individual zip file ?").
                setPositiveButton("Seperate Files", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        for (int i = 0; i < count; ++i) {
                            post(clipData.getItemAt(i).getUri(), null);
                        }
                    }
                }).
                setNegativeButton("Single File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        File zipFile = createZipFile();
                        ListItemData dataItem = addZipFileToList(zipFile);
                        new ZipTaskForClipData(zipFile, dataItem).execute(clipData);
                        //post(ZipUtil.createZip(clipData,createZipFile()));
                    }
                }).
                show();

    }

    public void initVariables() {
        filesListView = (ListView) findViewById(R.id.filesList);
        dataList = new ArrayList<ListItemData>();
        listAdapter = new ListAdapter(this, dataList);
        filesListView.setAdapter(listAdapter);
        filesListView.setEmptyView(findViewById(R.id.emptyList));
        addNewFile = (FloatingActionButton) findViewById(R.id.addNewFile);
        addNewImage = (FloatingActionButton) findViewById(R.id.addNewVideo);
        addNewVideo = (FloatingActionButton) findViewById(R.id.addNewImage);
        addNewFile.setOnClickListener(this);
        addNewImage.setOnClickListener(this);
        addNewVideo.setOnClickListener(this);
        mDbHelper = new DatabaseHelper(MainActivity.this);
        floatingMenu = (FloatingActionsMenu) findViewById(R.id.floatingMenu);
        dummyBg = findViewById(R.id.dummyBg);
        dummyBg.setOnClickListener(this);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        appIcons = new ArrayList<>();
        appNames = new ArrayList<>();
        packageToIndexMap = new ArrayList<>();
    }

    private void initListeners() {
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                dummyBg.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dummyBg.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        floatingMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                dummyBg.startAnimation(fadeIn);

            }

            @Override
            public void onMenuCollapsed() {
                dummyBg.startAnimation(fadeOut);
            }
        });

        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView url = (TextView) view.findViewById(R.id.fileUrl);
                //url.setSingleLine(false);
                if (url.getLineCount() == 1) {
                    url.setSingleLine(false);
                } else {
                    url.setSingleLine(true);

                }

            }
        });
    }


    public void initList() {

        new DataLoader().execute();

    }

    public Uri getFileUriFromContentUri() {
        return fileUriFromContentUri;
    }


    private class DataLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dataList.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            db = mDbHelper.getWritableDatabase();
            Cursor cursor = db.query(FileInfo.TABLE_NAME, FileInfo.PROJECTION, null, null, null, null, FileInfoColumns._ID + " DESC");

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    ListItemData data = new ListItemData();
                    data.fileName = cursor.getString(cursor.getColumnIndex(FileInfoColumns.FILENAME));
                    data.fileUrl = cursor.getString(cursor.getColumnIndex(FileInfoColumns.URL));
                    data.uploadStatus = cursor.getInt(cursor.getColumnIndex(FileInfoColumns.UPLOAD_STATUS));
                    data.rowId = cursor.getInt(cursor.getColumnIndex(FileInfoColumns._ID));
                    data.progress = cursor.getInt(cursor.getColumnIndex(FileInfoColumns.PROGRESS));
                    String path = cursor.getString(cursor.getColumnIndex(FileInfoColumns.URI_TO_UPLOAD));
                    if (path != null) {
                        data.uriToUpload = Uri.parse(path);
                    }
                    dataList.add(data);
                    cursor.moveToNext();

                } while (!cursor.isAfterLast());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void post(final Uri uri, final ListItemData pDataItem) {
        try {
            final File file = new File(uri.getPath());
            RequestParams params = new RequestParams();
            params.put(FILE_TO_UPLOAD, file);
            params.put(FILE_NAME, file.getName());
            client.post(SERVER_UPLOAD_URL_RELEASE, params, new AsyncHttpResponseHandler() {
                ListItemData dataItem;

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    dataItem.uploadStatus = UPLAOD_IN_PROGESS;
                    dataItem.progress = (int) ((bytesWritten * 100) / totalSize);
                    listAdapter.notifyDataSetChanged();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    dataItem.uploadStatus = UPLAOD_SUCCESS;
                    String response = new String(responseBody);
                    dataItem.fileUrl = response;
                    dataItem.fileName = response.substring(response.lastIndexOf("/") + 1);
                    dataItem.rowId = insertDataIntoDatabase(dataItem);
                    listAdapter.notifyDataSetChanged();
                    if (dataItem.fileName.contains("fileBasket")) {
                        try {
                            new File(EXTERNAL_STORAGE_DIR + dataItem.fileName).delete();
                        }catch(Exception e){

                        }
                    }


                }

                @Override
                public void onStart() {
                    super.onStart();
                    if (pDataItem == null) {
                        dataItem = new ListItemData();
                        dataItem.fileName = file.getName();
                        dataList.add(0, dataItem);
                    } else {
                        dataItem = pDataItem;
                    }

                    dataItem.progress = 0;
                    dataItem.uploadStatus = UPLAOD_CONNECTING;
                    listAdapter.notifyDataSetChanged();


                }



                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    //Toast.makeText(MainActivity.this, "Failed to upload.Try again later.", Toast.LENGTH_SHORT).show();
                    dataItem.uploadStatus = UPLAOD_FAILED;
                    dataItem.uriToUpload = uri;
                    dataItem.rowId = insertDataIntoDatabase(dataItem);
                    //patchDatabase(dataItem);
                    listAdapter.notifyDataSetChanged();

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void patch(final ListItemData dataItemParam) {
        try {
            final File file = new File(dataItemParam.uriToUpload.getPath());
            RequestParams params = new RequestParams();
            params.put(FILE_TO_UPLOAD, file);
            params.put(FILE_NAME, file.getName());
            client.post(SERVER_UPLOAD_URL_RELEASE, params, new AsyncHttpResponseHandler() {
                ListItemData dataItem;

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    dataItem.uploadStatus = UPLAOD_IN_PROGESS;
                    dataItem.progress = (int) ((bytesWritten * 100) / totalSize);
                    listAdapter.notifyDataSetChanged();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    dataItem.uploadStatus = UPLAOD_SUCCESS;
                    String response = new String(responseBody);
                    dataItem.fileUrl = response;
                    String localFileName = dataItem.fileName;
                    dataItem.fileName = response.substring(response.lastIndexOf("/") + 1);
                    dataItem.rowId = patchDatabase(dataItem);
                    listAdapter.notifyDataSetChanged();
                }

                @Override
                public void onStart() {
                    super.onStart();
                    dataItem = dataItemParam;
                    dataItem.uploadStatus = UPLAOD_CONNECTING;
                    listAdapter.notifyDataSetChanged();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Toast.makeText(MainActivity.this, "Failed to upload.Try again later.", Toast.LENGTH_SHORT).show();
                    dataItem.uploadStatus = UPLAOD_FAILED;
                    listAdapter.notifyDataSetChanged();

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private long patchDatabase(ListItemData data) {
        ContentValues values = new ContentValues();
        values.put(FileInfoColumns.UPLOAD_STATUS, data.uploadStatus);
        values.put(FileInfoColumns.URL, data.fileUrl);
        return db.update(FileInfo.TABLE_NAME, values, FileInfoColumns._ID + " = ?", new String[]{String.valueOf(data.rowId)});
    }

    private long insertDataIntoDatabase(ListItemData dataItem) {
        ContentValues values = new ContentValues();
        values.put(FileInfoColumns.FILENAME, dataItem.fileName);
        values.put(FileInfoColumns.UPLOAD_STATUS, dataItem.uploadStatus);
        values.put(FileInfoColumns.URL, dataItem.fileUrl);
        if (dataItem.uriToUpload != null) {
            values.put(FileInfoColumns.URI_TO_UPLOAD, dataItem.uriToUpload.getPath());
        }

        long rowId = db.insert(
                FileInfo.TABLE_NAME,
                null, values);
        return rowId;

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.terms) {
            new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).setTitle(getString(R.string.terms)).
                    setMessage(Html.fromHtml(getString(R.string.termsContent))).
                    setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    }).show();
            return true;
        }
        if (id == R.id.help) {
            new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).setTitle(getString(R.string.help)).
                    setMessage(Html.fromHtml(getString(R.string.helpContent))).
                    setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();

                        }
                    }).show();
            return true;
        } else if (id == R.id.deleteAll) {
            if (isNetworkAvailable(this) && (dataList.size() > 0)) {
                new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT).setTitle(getString(R.string.remove_all_files)).
                        setMessage(getString(R.string.are_you_sure)).
                        setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                deleteAllFiles();

                            }
                        }).
                        setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        }).show();

            } else {
                if (dataList.size() == 0) {
                    Toast.makeText(this, "No files to delete", Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void deleteAllFiles() {

        RequestParams params = new RequestParams();
        String fileNameList = "";
        int size = dataList.size();
        for (int i = 0; i < size; ++i) {
            fileNameList += dataList.get(i).fileName;
            if (i != (size - 1)) {
                fileNameList += ",";
            }
        }
        params.put(MainActivity.DELETE_FILE, true);
        params.put(MainActivity.FILE_NAME, fileNameList);
        MainActivity.client.post(MainActivity.SERVER_UPLOAD_URL_RELEASE, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d("responseBody", new String(responseBody));
                //Toast.makeText(MainActivity.this,new String(responseBody),Toast.LENGTH_LONG);
                dataList.clear();
                listAdapter.notifyDataSetChanged();
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        MainActivity.db.delete(FileInfo.TABLE_NAME, null, null);
                    }
                }.start();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(MainActivity.this, "Failed to delete files.Try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PICKFILE_REQUEST_CODE:
                    floatingMenu.collapse();
                    // For JellyBean and above
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ClipData clip = data.getClipData();

                        if (clip != null) {
                            if (clip.getItemCount() == 1) {
                                post(clip.getItemAt(0).getUri(), null);
                            } else {
                                initDialog(clip);
                            }
                        }
                        // For Ice Cream Sandwich
                    } else {
                        ArrayList<String> paths = data.getStringArrayListExtra
                                (FilePickerActivity.EXTRA_PATHS);
                        if (paths != null) {
                            if (paths.size() == 1) {
                                post(Uri.parse(paths.get(0)), null);
                            } else {
                                initDialogWithPath(paths);
                            }
                        }
                    }

                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.addNewFile:
                Intent i = new Intent(this, FilePickerActivity.class);
                // This works if you defined the intent filter
                // Intent i = new Intent(Intent.ACTION_GET_CONTENT);

                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
                startActivityForResult(i, PICKFILE_REQUEST_CODE);
              /*  Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image*//*");
                //intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, PICKFILE_RESULT_CODE);*/
                break;
            case R.id.addNewImage:
                new Picker.Builder(this, this, R.style.MIP_theme)
                        .setPickMode(Picker.PickMode.MULTIPLE_IMAGES)
                        .disableCaptureImageFromCamera()
                        .build()
                        .startActivity();
             /*  Intent imageIntent  = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                imageIntent.setType("image*//*");
                imageIntent.addCategory(Intent.CATEGORY_OPENABLE);
                imageIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(imageIntent,PICKIMAGE_REQUEST_CODE);*/
                break;
            case R.id.addNewVideo:
                new Picker.Builder(this, this, R.style.MIP_theme)
                        .setPickMode(Picker.PickMode.MULTIPLE_VIDEOS)
                        .disableCaptureImageFromCamera()
                        .build()
                        .startActivity();
                /*Intent videoIntent  = new Intent(Intent.ACTION_PICK);
                videoIntent.setType("video*//*");
                videoIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(videoIntent,PICKVIDEO_REQUEST_CODE);*/
                break;

            case R.id.dummyBg:
                dummyBg.startAnimation(fadeOut);
                floatingMenu.collapse();
                break;

        }

    }

    @Override
    public void onPickedSuccessfully(ArrayList<ImageEntry> arrayList) {
        floatingMenu.collapse();
        if (arrayList.size() == 1) {
            post(Uri.parse(arrayList.get(0).path), null);
        } else {
            initDialog(arrayList);

        }
    }

    @Override
    public void onCancel() {

    }


    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public boolean isPackageInstalled(Context context,String packagename) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
