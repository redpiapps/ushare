package redpi.apps.tables;

import android.provider.BaseColumns;

/**
 * Created by Nikhil on 01-08-2015.
 */
public class FileInfo {

    public static final String TABLE_NAME = "FileInfo";

    public FileInfo() {
    }

    public static String[] PROJECTION = {
            FileInfoColumns._ID,
            FileInfoColumns.FILENAME,
            FileInfoColumns.URL,
            FileInfoColumns.UPLOAD_STATUS,
            FileInfoColumns.URI_TO_UPLOAD,
            FileInfoColumns.PROGRESS
    };

    public static abstract class FileInfoColumns implements BaseColumns {
        public static final String FILENAME = "file_name";
        public static final String URL = "url";
        public static final String UPLOAD_STATUS = "upload_status";
        public static final String URI_TO_UPLOAD = "uri_to_upload";
        public static final String PROGRESS = "progress";



    }


}
