package redpi.apps.utilities;

import android.content.ClipData;
import android.net.Uri;
import android.os.Environment;

import net.yazeed44.imagepicker.model.ImageEntry;

import org.apache.commons.io.FileUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Nikhil on 03-08-2015.
 */
public class ZipUtil {

    public static Uri createZip(ArrayList<ImageEntry> arrayList,File zipFile) {
        int fileCount = arrayList.size();
        if (zipFile.exists()) {
            zipFile.delete();
        }
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
            try {
                for (int i = 0; i < fileCount; ++i) {
                    ImageEntry entry = arrayList.get(i);
                    File fileToZip = new File(entry.path);
                    String filename = fileToZip.getName();
                    byte[] bytes = FileUtils.readFileToByteArray(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(filename);
                    zos.putNextEntry(zipEntry);
                    zos.write(bytes);
                    zos.closeEntry();
                }
            } catch (IOException ioe) {

            } finally {
                try {
                    ;
                    zos.close();
                } catch (IOException ioe) {

                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Uri.fromFile(zipFile);

    }

    public static Uri createZipWithPaths(ArrayList<String> arrayList,File zipFile) {
        int fileCount = arrayList.size();
        if (zipFile.exists()) {
            zipFile.delete();
        }
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
            try {
                for (int i = 0; i < fileCount; ++i) {
                    String path = arrayList.get(i);
                    File fileToZip = new File(path);
                    String filename = fileToZip.getName();
                    byte[] bytes = FileUtils.readFileToByteArray(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(filename);
                    zos.putNextEntry(zipEntry);
                    zos.write(bytes);
                    zos.closeEntry();
                }
            } catch (IOException ioe) {

            } finally {
                try {
                    ;
                    zos.close();
                } catch (IOException ioe) {

                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Uri.fromFile(zipFile);

    }

    public static Uri createZip(ClipData clipDataList,File zipFile) {
        int fileCount = clipDataList.getItemCount();
        if (zipFile.exists()) {
            zipFile.delete();
        }
        try {
            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
            try {
                for (int i = 0; i < fileCount; ++i) {
                    Uri uri = clipDataList.getItemAt(i).getUri();
                    File fileToZip = new File(uri.getPath());
                    String filename = fileToZip.getName();
                    byte[] bytes = FileUtils.readFileToByteArray(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(filename);
                    zos.putNextEntry(zipEntry);
                    zos.write(bytes);
                    zos.closeEntry();
                }
            } catch (IOException ioe) {

            } finally {
                try {
                    ;
                    zos.close();
                } catch (IOException ioe) {

                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Uri.fromFile(zipFile);

    }



}
